package main

import (
	"context"
	cryptorand "crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"golang.org/x/crypto/acme"

	"github.com/miekg/dns"

	"gitlab.com/yumeko/config"
)

var configINI = config.NewINI()
var configINIPath = config.NewString("letsdns.ini", config.NewMTime(configINI))
var configRoot = config.NewInstance(configINIPath)

var algo string
var server string
var keyname string
var key string
var ttl time.Duration
var root string
var domain string

var keyfile string
var certfile string
var staging bool
var cachefile string
var contact string

func init() {
	configINI.Attach(`[dns]
# DNS server to update
server=127.0.0.1:53`, config.NewPointer(&server))
	configINI.Attach(`[dns]
# Name of the key as a FQDN (Including trailing dot)
name=mykey.`, config.NewPointer(&keyname))
	configINI.Attach(`[dns]
# Key value
key=WouldntYouWantToKnow`, config.NewPointer(&key))
	configINI.Attach(`[dns]
# TTL of records to be created
ttl=5m`, config.NewPointer(&ttl))
	configINI.Attach(`[dns]
# Root zone to be updated as a FQDN
root=my.domain.com.`, config.NewPointer(&root))
	configINI.Attach(`[dns]
# Domain to be updated as a FQDN
domain=my.domain.com.`, config.NewPointer(&domain))
	configINI.Attach(`[dns]
# Algorithm
algo=md5`, config.NewPointer(func(v string) error {
		switch strings.ToLower(v) {
		case "hmac-md5", "md5":
			algo = dns.HmacMD5
		case "hmac-sha1", "sha1":
			algo = dns.HmacSHA1
		case "hmac-sha256", "sha256":
			algo = dns.HmacSHA256
		case "hmac-sha512", "sha512":
			algo = dns.HmacSHA512
		default:
			return fmt.Errorf("Unknown alogrithm: %v", algo)
		}
		return nil
	}))
	configINI.Attach(`[letsencrypt]
# Cache file
cache = .letsdns`, config.NewPointer(&cachefile))
	configINI.Attach(`[letsencrypt]
# Keyfile
key = `, config.NewPointer(&keyfile))
	configINI.Attach(`[letsencrypt]
# certfile
cert = `, config.NewPointer(&certfile))
	configINI.Attach(`[letsencrypt]
# Set to false to get a real cert
# When false there is a highly restrictive rate-limit
staging=true`, config.NewPointer(&staging))
	configINI.Attach(`[letsencrypt]
# Email contact
contact=`, config.NewPointer(&contact))
}

func getIP(url string) (net.IP, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bb, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	ip := net.ParseIP(string(bb))
	if ip == nil {
		return nil, fmt.Errorf("%q is not an IP address", string(bb))
	}
	return ip, nil
}

func UpdateDNS(rrset []dns.RR) error {
	cnt := &dns.Client{
		Net:        "tcp",
		TsigSecret: map[string]string{keyname: key},
	}
	m := &dns.Msg{}
	rrr := make([]dns.RR, len(rrset))
	for i, rr := range rrset {
		rrr[i] = &dns.ANY{dns.RR_Header{
			Name:   rr.Header().Name,
			Rrtype: rr.Header().Rrtype,
			Class:  rr.Header().Class,
		}}
	}
	m.RemoveRRset(rrr)
	m.SetUpdate(root)
	m.Insert(rrset)
	m.SetTsig(keyname, algo, 300, time.Now().Unix())
	_, _, err := cnt.Exchange(m, server)
	if err != nil {
		return err
	}
	return nil
}

func main() {
	if len(os.Args) > 1 {
		configINIPath.Change(os.Args[1])
	}
	errs := configRoot.Reload()
	if len(errs) > 0 {
		fmt.Println("Fatal error:")
		for _, err := range errs {
			fmt.Println(err)
		}
		return
	}
	v4, err := getIP("http://v4.ident.me/")
	if err != nil {
		panic(err)
	}
	v6, err := getIP("http://v6.ident.me/")
	if err != nil {
		panic(err)
	}
	fmt.Println(v6, v4)
	err = UpdateDNS([]dns.RR{
		&dns.A{
			Hdr: dns.RR_Header{
				Name:   domain,
				Rrtype: dns.TypeA,
				Class:  dns.ClassINET,
				Ttl:    uint32(ttl.Seconds()),
			},
			A: v4,
		},
		&dns.AAAA{
			Hdr: dns.RR_Header{
				Name:   domain,
				Rrtype: dns.TypeAAAA,
				Class:  dns.ClassINET,
				Ttl:    uint32(ttl.Seconds()),
			},
			AAAA: v6,
		},
	})
	if err != nil {
		panic(err)
	}
	if certfile == "" || keyfile == "" {
		return
	}
	pub, _, err := ReadCertKeyPair(certfile, keyfile)
	now := time.Now()
	if err == nil && now.After(pub.NotBefore) && now.Before(pub.NotAfter.Add(-time.Hour*24*30)) {
		return
	}
	cache := struct {
		Key     *rsa.PrivateKey
		Account *acme.Account
	}{}
	bcc, err := ioutil.ReadFile(cachefile)
	if !os.IsNotExist(err) {
		if err != nil {
			panic(err)
		}
		err = json.Unmarshal(bcc, &cache)
		if err != nil {
			panic(err)
		}
	}
	func() {
		defer func() {
			fi, err := os.Create(cachefile)
			if err != nil {
				panic(err)
			}
			defer fi.Close()
			err = json.NewEncoder(fi).Encode(cache)
			if err != nil {
				panic(err)
			}
		}()
		client := &acme.Client{
			Key:          cache.Key,
			DirectoryURL: "https://acme-staging.api.letsencrypt.org/directory",
		}
		if !staging {
			client.DirectoryURL = "https://acme-v01.api.letsencrypt.org/directory"
		}
		if cache.Key == nil || cache.Account == nil {
			fmt.Println("Registering new account")
			if contact == "" {
				panic("Contact must be valid")
			}
			key, err := rsa.GenerateKey(cryptorand.Reader, 2048)
			if err != nil {
				panic(err)
			}
			client.Key = key
			account, err := client.Register(context.Background(), &acme.Account{
				Contact: []string{"mailto:" + contact},
			}, acme.AcceptTOS)
			if err != nil {
				panic(err)
			}
			cache.Key = key
			cache.Account = account
		}
		key, err := rsa.GenerateKey(cryptorand.Reader, 4096)
		if err != nil {
			panic(err)
		}
		ledomain := strings.TrimSuffix(domain, ".")
		csr, err := x509.CreateCertificateRequest(cryptorand.Reader, &x509.CertificateRequest{
			Subject: pkix.Name{
				CommonName: ledomain,
			},
		}, key)
		if err != nil {
			panic(err)
		}
		auth, err := client.Authorize(context.Background(), ledomain)
		if err != nil {
			panic(err)
		}
		chal := func() *acme.Challenge {
			for _, c := range auth.Challenges {
				if c.Type == "dns-01" {
					return c
				}
			}
			panic("Unable to find dns challenge")
		}()
		tvalue, err := client.DNS01ChallengeRecord(chal.Token)
		if err != nil {
			panic(err)
		}
		err = UpdateDNS([]dns.RR{
			&dns.TXT{
				Hdr: dns.RR_Header{
					Name:   "_acme-challenge." + domain,
					Rrtype: dns.TypeTXT,
					Class:  dns.ClassINET,
					Ttl:    uint32(ttl.Seconds()),
				},
				Txt: []string{tvalue},
			},
		})
		if err != nil {
			panic(err)
		}
		_, err = client.Accept(context.Background(), chal)
		if err != nil {
			panic(err)
		}
		_, err = client.WaitAuthorization(context.Background(), auth.URI)
		if err != nil {
			panic(err)
		}
		der, _, err := client.CreateCert(context.Background(), csr, 0, true)
		if err != nil {
			panic(err)
		}
		err = WritePrivate(key, keyfile)
		if err != nil {
			panic(err)
		}
		err = WriteCert(der, certfile)
		if err != nil {
			panic(err)
		}
	}()
	os.Exit(2)
}
