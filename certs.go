package main

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func ReadPEM(file, suffix string) ([]*pem.Block, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	out := []*pem.Block{}
	for {
		var blk *pem.Block
		blk, data = pem.Decode(data)
		if blk == nil {
			break
		}
		if strings.HasSuffix(blk.Type, suffix) {
			out = append(out, blk)
		}
	}
	if len(out) == 0 {
		return nil, fmt.Errorf("no blocks in PEM file")
	}
	if len(data) != 0 {
		return nil, fmt.Errorf("garbage after end of PEM file")
	}
	return out, nil
}

func ReadCertKeyPair(certfi, keyfi string) (*x509.Certificate, crypto.PrivateKey, error) {
	cert, err := ReadPEM(certfi, "CERTIFICATE")
	if err != nil {
		return nil, nil, err
	}
	key, err := ReadPEM(keyfi, "PRIVATE KEY")
	if err != nil {
		return nil, nil, err
	}
	pub, err := x509.ParseCertificate(cert[0].Bytes)
	if err != nil {
		return nil, nil, err
	}
	priv, err := parsePrivateKey(key[0].Bytes)
	if err != nil {
		return nil, nil, err
	}
	switch pub := pub.PublicKey.(type) {
	case *rsa.PublicKey:
		priv, ok := priv.(*rsa.PrivateKey)
		if !ok || priv.N.Cmp(pub.N) != 0 {
			return nil, nil, fmt.Errorf("non matching certs")
		}
	case *ecdsa.PublicKey:
		priv, ok := priv.(*ecdsa.PublicKey)
		if !ok || priv.X.Cmp(pub.X) != 0 || priv.Y.Cmp(pub.Y) != 0 {
			return nil, nil, fmt.Errorf("non matching certs")
		}
	default:
		return nil, nil, fmt.Errorf("unknown algorithm")
	}
	return pub, priv, nil
}

//crypto/tls
func parsePrivateKey(der []byte) (crypto.PrivateKey, error) {
	if key, err := x509.ParsePKCS1PrivateKey(der); err == nil {
		return key, nil
	}
	if key, err := x509.ParsePKCS8PrivateKey(der); err == nil {
		switch key := key.(type) {
		case *rsa.PrivateKey, *ecdsa.PrivateKey:
			return key, nil
		default:
			return nil, errors.New("tls: found unknown private key type in PKCS#8 wrapping")
		}
	}
	if key, err := x509.ParseECPrivateKey(der); err == nil {
		return key, nil
	}
	return nil, errors.New("tls: failed to parse private key")
}

func WriteCert(derBytes [][]byte, file string) error {
	fi, err := os.Create(file)
	if err != nil {
		return err
	}
	defer fi.Close()
	for _, d := range derBytes {
		err := pem.Encode(fi, &pem.Block{
			Type:  "CERTIFICATE",
			Bytes: d,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func WritePrivate(pkey crypto.PrivateKey, file string) error {
	fi, err := os.Create(file)
	if err != nil {
		return err
	}
	defer fi.Close()
	switch pkey := pkey.(type) {
	case *rsa.PrivateKey:
		return pem.Encode(fi, &pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: x509.MarshalPKCS1PrivateKey(pkey),
		})
	case *ecdsa.PrivateKey:
		ec, err := x509.MarshalECPrivateKey(pkey)
		if err != nil {
			return err
		}
		return pem.Encode(fi, &pem.Block{
			Type:  "EC PRIVATE KEY",
			Bytes: ec,
		})
	default:
		panic("unknown private key type")
	}
}
